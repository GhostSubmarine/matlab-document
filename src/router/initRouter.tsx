import LoginPage from 'pages/common/login'
import MainPage from 'pages/layout'
import { HashRouter, Route, Routes } from 'react-router-dom'


const InitRouter = () => 
  <HashRouter>
    <Routes>
      <Route path={'/'} element={<MainPage />} />
      <Route path={'/login'} element={<LoginPage />} />
    </Routes>
  </HashRouter>

export default InitRouter 