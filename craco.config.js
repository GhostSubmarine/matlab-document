const path = require('path')

module.exports = {
  webpack: {
    configure: (webpackConfig, { env, paths }) => {
      // webpackConfig.plugins[0].userOptions.template = './templatePublic/index.html'
      // console.log(webpackConfig.plugins[0].userOptions)
      // const plugins0 = new HtmlWebpackPlugin({
      //   template: './templatePublic/index.html',
      //   filename: 'index.html', //打包后的文件名
      //   minify: {
      //     removeAttributeQuotes: false, //是否删除属性的双引号
      //     collapseWhitespace: false, //是否折叠空白
      //   },
      //   // hash: true //是否加上hash，默认是 false
      // })
      // webpackConfig.plugins[0] = plugins0
      console.log(webpackConfig.output)
      webpackConfig.plugins[0].userOptions.template = './templatePublic/index.html'
      webpackConfig.output.path = path.resolve(__dirname, 'public')
      return webpackConfig
    }
  },
  style: {
    postcssOptions: {
      plugins: [
        require('tailwindcss'),
        require('autoprefixer')
      ],
    },
  },
}